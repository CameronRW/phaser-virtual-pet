import Phaser from 'phaser';

import backyardImage from './assets/images/backyard.png';
import appleImage from './assets/images/apple.png';
import candyImage from './assets/images/candy.png';
import rotateImage from './assets/images/rotate.png';
import toyImage from './assets/images/rubber_duck.png';

// create a new scene
const gameScene = new Phaser.Scene('Game');

// some parameters for our scene
gameScene.init = function init() {};

// load asset files for our game
gameScene.preload = function preload() {
  // load assets
  this.load.image('backyard', backyardImage);
  this.load.image('apple', appleImage);
  this.load.image('candy', candyImage);
  this.load.image('rotate', rotateImage);
  this.load.image('toy', toyImage);
};

// executed once, after assets were loaded
gameScene.create = function create() {};

// our game's configuration
const config = {
  type: Phaser.AUTO,
  width: 360,
  height: 640,
  scene: gameScene,
  title: 'Virtual Pet',
  pixelArt: false,
  backgroundColor: 'ffffff',
};

// create the game, and pass it the configuration
// eslint-disable-next-line no-unused-vars
const game = new Phaser.Game(config);
